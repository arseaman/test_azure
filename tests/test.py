import allure
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC



@allure.suite('First Suite')
@allure.story('First Story')
@allure.title('Test Google Search Positive')
def test_positive(driver):
    with allure.step('Step 1'):
        driver.get('https://google.com/')
    with allure.step('Step 2'):
        driver.find_element(By.CSS_SELECTOR, "button[id=L2AGLb]").click()
    with allure.step('Step 3'):
        search = driver.find_element(By.CSS_SELECTOR, "textarea[name=q]")
    sleep(5)
    with allure.step('Step 4'):
        search.send_keys('weather')
        search.send_keys(Keys.ENTER)
    sleep(5)
    with allure.step('Step 5'):
        image_assert = driver.find_element(By.CSS_SELECTOR, "img[alt=Google]")
        assert image_assert.is_displayed()
    sleep(1)


@allure.story('First Story')
@allure.title('Test Google Search Negative')
def test_negative(driver):
    with allure.step('Step 1'):
        driver.get('https://google.com/')
    with allure.step('Step 2'):
        driver.find_element(By.CSS_SELECTOR, "button[id=L2AGLb]").click()
    with allure.step('Step 3'):
        search = driver.find_element(By.CSS_SELECTOR, "textarea[name=q]")
    sleep(5)
    with allure.step('Step 4'):
        search.send_keys('weather')
        search.send_keys(Keys.ENTER)
    sleep(5)
    with allure.step('Step 5'):
        assert search.text == 'eather'
    sleep(1)
