import datetime as datetime
import allure
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.options import Options as ChromeOptions


@pytest.fixture()
def driver():
    options = ChromeOptions()
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    options.add_argument("--disable-blink-features=AutomationControlled")
    server = 'http://134.122.72.126:4444/wd/hub'
    driver = webdriver.Remote(command_executor=server, options=options)
    driver.implicitly_wait(10)
    driver.maximize_window()
    yield driver
    attach = driver.get_screenshot_as_png()
    allure.attach(attach, name=f"Screenshot{datetime.datetime.now()}", attachment_type=allure.attachment_type.PNG)
    driver.quit()



# @pytest.fixture()
# def driver():
#     # ChromeDriverManager().install()
#     options = Options()
#     options.add_experimental_option("excludeSwitches", ["enable-automation"])
#     options.add_experimental_option('useAutomationExtension', False)
#     options.add_argument("--disable-blink-features=AutomationControlled")
#     driver = webdriver.Chrome(options=options)
#     driver.maximize_window()
#     yield driver
#     attach = driver.get_screenshot_as_png()
#     allure.attach(attach, name=f"Screenshot{datetime.datetime.now()}", attachment_type=allure.attachment_type.PNG)
#     driver.quit()


